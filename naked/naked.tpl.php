<?php
/**
 * @file
 * Template McLean panels layout.
 *
 * This template effectively removes all the goofy Panels' markup.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   $content['middle']: The only panel in the layout.
 */
?>
<?php print $content['middle']; ?>
