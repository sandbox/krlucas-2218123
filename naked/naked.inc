<?php
/**
 * @file
 * cTools Plug-in definition for custom Panels layout.
 */

$plugin = array(
  'title' => t('Naked'),
  'category' => t('Naked'),
  'icon' => 'naked.png',
  'theme' => 'naked',
  'css' => 'naked.css',
  'regions' => array('middle' => t('Middle column')),
);
