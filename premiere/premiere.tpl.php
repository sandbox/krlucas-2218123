<?php
/**
 * @file
 * Template McLean panels layout.
 *
 * This template effectively removes all the goofy Panels' markup.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   $content['top']: The stuff at the top of the Panels content area
 *   $content['middle']: The stuff at the bottom of the Panels content area.
 */
?>
<?php print $content['top']; ?>
<?php print $content['middle']; ?>
