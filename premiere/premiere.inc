<?php
/**
 * @file
 * cTools Plug-in definition for Premiere Program Panels layout.
 */

$plugin = array(
  'title' => t('Premiere Program'),
  'category' => t('Naked'),
  'icon' => 'premiere.png',
  'theme' => 'premiere',
  'css' => 'premiere.css',
  'regions' => array(
    'top' => t('First Row, Middle Column.'),
    'middle' => t('Second Row, Middle Column'),
  ),
);
