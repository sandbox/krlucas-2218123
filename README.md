This is not a module or a theme in and of itself.

This project contains two examples of extremely simple one "column" Panels' layouts that
introduce very little Panels' specific markup.

- Naked: A layout with most HTML stripped, one "column", one "row".
- Premiere: A layout with most HTML stripped, one "column", two "rows". There's no HTML
wrapping the "rows" (named "top" and "middle")--you'll have to add that yourself.

To use a layout, copy and paste the contents of the layout into your theme. For
instance into the directory:
>sites/all/themes/mytheme/templates/panels/naked

Then edit your theme .info file and add the following line:
>plugins[panels][layouts] = templates/panels

If you want to customize or eliminate that "panel-pane" div wrappers that surround the 
Drupal content elements added to the Panel you'll need to also copy and paste 
"panels-pane.tpl.php" into your theme templates directory. Other non-Panels' markup will
remain.

If you want to remove the "panels-separator" divs, you need to do that through the admin.
Go to Admin > Pages. Select the Panel controller you want to modify. For each variant,
go to the Content tab. For each Pane in the Variant, click the "Gear" icon and under
style settings choose "No Markup at All".